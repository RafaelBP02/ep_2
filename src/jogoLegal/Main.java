package jogoLegal;
import java.awt.Color;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {
		JFrame obj = new JFrame();	
		Gameplay gameplay = new Gameplay();
		
		
		obj.setBounds(10, 10, 800, 650);
		obj.setBackground(new Color(0, 0, 26));
		obj.setResizable(false);
		obj.setVisible(true);
		obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		obj.add(gameplay);
		
		
	}

}
