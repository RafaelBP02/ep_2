<h1>EP_2: Jogo da Cobrinha


<h2>Intalaçao
<p>	0.O programa foi feito usando o Java 11.0.4<p/>
<p>	1.A pessoa deve possuir o Eclipse</p>
<p>	2.Após baixar o arquivo do git lab a pessoa deverá acrescentá-lo como um projeto e pedir para o eclipse rodar o código Main.java(Ep2/src/JogoLegal/Main.java) </p>
<p><strong>3.PS: Caso as frutas não aparecam no inicio do jogo feche e execute o jogo novamente<strong></p>

<h2>Como Jogar
<p>	O Usuário irá utiliza as teclas  cima, baixo, esquerda, direita para movimentar a cobrinha nessas respectivas posiçoes</p>
<p>	Ao iniciar o jogo a cobrinha estará parada. Então, para iniciar sua movimentação o usuário devera apertar cima, baixo ou direita<p>

<h2>Mecânicas do jogo:
<p>	A cobrinha, ao se aproximar da parede irá aparecer do outro lado</p>
<p>	Existem barreiras fixas de fogo no mapa que impedem as cobrinhas de atravessarem</p>
<p>	O jogador <strong>pode mudar de cobrinha<strong> apertando as teclas: 1 para virar a padrao, 2 para a kitty ou 3 para a star</p>
<p>Cobrinhas:</p>
<p>	Basica(1):</p>
<p>		A Basica ganha pontuação normalmente e possui nenhum poder</p>
<p>	Kitty(2):</p>
<p>		A Kitty é <strong>imune às barreiras de fogo<strong>,não morre pra elas, e ganha pontuação normalmente.</p>
<p>	Star(3):</p>
<p>		A cobrinha <strong>ganha  o dobro de pontuação<strong> e anda normalmente</p>
<p>	Ao iniciar o jogo o Usuário irá se deparar com 3 modelos de frutas ou uma bomba que irão aparecer aleatóriamente e sumir apos alguns movimentos da cobra</p>
<p>Maça: </p>
<p>	Ao colidir com esse elemento a cobrinha ganha 1 ponto e 1 de tamanho</p></p>
<p>Laranja: </p>
<p>	Ao colidir com esse elemento a cobrinha ganha 2 pontos e 2 de tamanho</p></p>
<p>Maçã Podre: </p>
<p>	Ao colidir com esse elemento a cobrinha tem seu tamanho resetado. Tenha cuidado pois essa fruta é parecida com a maça normal</p>
<p>	Essa fruta irá sumir após alguns movimentos</p>
<p><strong>GAME OVER<strong>: </p>
<p>	A cobrinha irá morrer caso ela colida com o proprio corpo</p>
<p>	Ela também irá morrer se caso ela colida com a <strong>Bomba<strong>(Essa bomba irá sumir após alguns movimentos)</p></p></p>

<h2>Diagrama UML
<p></p>
[Link para visualização do diagrama](https://drive.google.com/file/d/1x3J-S2Azo59o1i-iaE89uugI5LucHEP_/view?usp=sharing)

<h3>Aluno:
<p>NOME: Rafael Berto Pereira</p>
<p>MATRÍCULA: 180108344</p>


<p>PS: TODAS A SPRITES DO JOGO FORAM FEITAS POR MIM USANDO O PROGRAMA ASEPRITE.
