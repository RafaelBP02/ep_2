package jogoLegal;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;


public class Gameplay extends JPanel implements KeyListener, ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int[] tamanhoXCobrinha = new int[725];
	private int[] tamanhoYCobrinha = new int[575];
	
	private int movimento = 0;
	private int contador = 0;
	
	private boolean oeste = false;
	private boolean leste = false;
	private boolean norte = false;
	private boolean sul = false;
	
	//barreira
	private ImageIcon pegandoFogo;
	private int [] barreirax = {75, 100, 125, 150, 175, 200, 225, 250, 275, 500, 525, 550, 575, 600, 625, 650, 675, 700};
	
	private int [] barreiray = {50, 75, 100, 125, 150, 175, 400, 425, 450, 475, 500, 525};
	
	//cobrinha
	private ImageIcon cabecaOeste;
	private ImageIcon cabecaLeste;
	private ImageIcon cabecaNorte;
	private ImageIcon cabecaSul;
	
	private ImageIcon corpoCobrinha;
	
	private int tamanhoCobrinha = 3;
	
	private Timer timer;
	private int delay = 100;
	
	private int [] frutaPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350,
			400,425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725};
	
	private int [] frutaPosicaoy = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375,
			400,425, 450, 475, 500, 525, 550,575};
	
	private int [] frutaID = {1,2,3,4};
	private int cobrinhaID = 1;
	
	private ImageIcon imagemMaca;
	private ImageIcon imagemLaranja;
	private ImageIcon imagemMacaPodre;
	private ImageIcon imagemBomba;
	
	private Random random = new Random();
	
	private int proxX = random.nextInt(27);
	private int proxY = random.nextInt(22);
	private int proxFruta = random.nextInt(4);
	
	//pontuacao
	private int pontuacao = 0;
	
	
	public Gameplay(){
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay, this);
		timer.start();
			
	}
	
	public void paint(Graphics g) {
		if(movimento == 0) {
			tamanhoXCobrinha[2] = 50;
			tamanhoXCobrinha[1] = 75;
			tamanhoXCobrinha[0] = 100;
			
			tamanhoYCobrinha[2] = 100;
			tamanhoYCobrinha[1] = 100;
			tamanhoYCobrinha[0] = 100;
			
		}
		
		//fazendo a borda para o jogo
		g.setColor(Color.WHITE);
		g.drawRect(18, 9, 751,601);
		 
		
		//fazendo o plano de fundo
		g.setColor(new Color(132, 135, 170));
		g.fillRect(19, 10, 750, 600);
		
		
		//fazendo pontuacao aparecer
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Pontos: "+pontuacao, 40, 30);
		
		
		//Informar o tamanho da cobrinha
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Comprimento: "+tamanhoCobrinha, 40, 50);
		
		//implementando Barreira
		pegandoFogo = new ImageIcon("Fogo_Barreira.png");
		for(int j = 0; j < 6; j++) {
			pegandoFogo.paintIcon(this, g, 375, 75+25*j);
		}
		for(int j = 0; j < 6; j++) {
			pegandoFogo.paintIcon(this, g, 375, 400+25*j);
		}
		for(int j = 0; j < 8; j++) {
			pegandoFogo.paintIcon(this, g, 75+25*j, 275);
		}
		for(int j = 0; j < 8; j++) {
			pegandoFogo.paintIcon(this, g, 500+25*j, 275);
		}
		
		
		//desenhando as cobrinhas
		
		if(cobrinhaID == 1) {
			
			cabecaLeste = new ImageIcon("Circular_snakeL.png");
			cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[0], tamanhoYCobrinha[0]);
			
			for(int a = 0; a<tamanhoCobrinha; a++) {
				if(a == 0 && leste) {
					cabecaLeste = new ImageIcon("Circular_snakeL.png");
					cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && oeste) {
					cabecaOeste = new ImageIcon("Circular_snakeO.png");
					cabecaOeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && sul) {
					cabecaSul = new ImageIcon("Circular_snakeS.png");
					cabecaSul.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && norte) {
					cabecaNorte = new ImageIcon("Circular_snake.png");
					cabecaNorte.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a != 0) {
					corpoCobrinha = new ImageIcon("Circular_snake_body.png");
					corpoCobrinha.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
							
			}
		}
		else if(cobrinhaID == 2) {
			cabecaLeste = new ImageIcon("kittyL.png");
			cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[0], tamanhoYCobrinha[0]);
			
			for(int a = 0; a<tamanhoCobrinha; a++) {
				if(a == 0 && leste) {
					cabecaLeste = new ImageIcon("kittyL.png");
					cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && oeste) {
					cabecaOeste = new ImageIcon("kittyO.png");
					cabecaOeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && sul) {
					cabecaSul = new ImageIcon("kittyS.png");
					cabecaSul.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && norte) {
					cabecaNorte = new ImageIcon("kitty.png");
					cabecaNorte.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a != 0) {
					corpoCobrinha = new ImageIcon("kitty_body.png");
					corpoCobrinha.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
							
			}
		}
		else if(cobrinhaID == 3) {
			cabecaLeste = new ImageIcon("Star_snakeL.png");
			cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[0], tamanhoYCobrinha[0]);
			
			for(int a = 0; a<tamanhoCobrinha; a++) {
				if(a == 0 && leste) {
					cabecaLeste = new ImageIcon("Star_snakeL.png");
					cabecaLeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && oeste) {
					cabecaOeste = new ImageIcon("Star_snakeO.png");
					cabecaOeste.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && sul) {
					cabecaSul = new ImageIcon("Star_snakeS.png");
					cabecaSul.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a == 0 && norte) {
					cabecaNorte = new ImageIcon("Star_snake.png");
					cabecaNorte.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
				if(a != 0) {
					corpoCobrinha = new ImageIcon("Star_snake_body.png");
					corpoCobrinha.paintIcon(this, g, tamanhoXCobrinha[a], tamanhoYCobrinha[a]);
				}
							
			}
		}
		
		
		//Implementando as frutas e bomba
		if(frutaID[proxFruta] == 1) {
		imagemMaca = new ImageIcon("maca_grande.png");
		
		if((frutaPosicaox[proxX] == tamanhoXCobrinha[0] && frutaPosicaoy[proxY] == tamanhoYCobrinha[0])) {
			if(cobrinhaID != 3) {
				pontuacao++;
				tamanhoCobrinha++;
				proxX = random.nextInt(27);
				proxY = random.nextInt(22);
				proxFruta = random.nextInt(4);	
			}
			else if(cobrinhaID == 3) {
				pontuacao = pontuacao + 2;
				tamanhoCobrinha = tamanhoCobrinha + 2;
				proxX = random.nextInt(27);
				proxY = random.nextInt(22);
				proxFruta = random.nextInt(4);	
			}
		}
		
			imagemMaca.paintIcon(this, g, frutaPosicaox[proxX], frutaPosicaoy[proxY]);
			
			if(contador >= 12) {
				proxX = random.nextInt(27);
				proxY = random.nextInt(22);
				proxFruta = random.nextInt(4);	
				contador = 0;
				
			}
		}
		else if(frutaID[proxFruta] == 2) {
			
			imagemLaranja = new ImageIcon("laranja_grande.png");
			
			if((frutaPosicaox[proxX] == tamanhoXCobrinha[0] && frutaPosicaoy[proxY] == tamanhoYCobrinha[0])) {
				if(cobrinhaID != 3) {
					pontuacao = pontuacao + 2;
					tamanhoCobrinha = tamanhoCobrinha + 2;
					proxX = random.nextInt(27);
					proxY = random.nextInt(22);
					proxFruta = random.nextInt(4);	
				}
				else if(cobrinhaID == 3) {
					pontuacao = pontuacao + 4;
					tamanhoCobrinha = tamanhoCobrinha + 4;
					proxX = random.nextInt(27);
					proxY = random.nextInt(22);
					proxFruta = random.nextInt(4);
				}
			}
			
				imagemLaranja.paintIcon(this, g, frutaPosicaox[proxX], frutaPosicaoy[proxY]);	
				
				if(contador >= 7) {
					proxX = random.nextInt(27);
					proxY = random.nextInt(22);
					proxFruta = random.nextInt(4);	
					contador = 0;
					
				}
		}
		else if(frutaID[proxFruta] == 3 && pontuacao > 0) {
						
			imagemMacaPodre = new ImageIcon("maca_g_podre.png");
				
			if((frutaPosicaox[proxX] == tamanhoXCobrinha[0] && frutaPosicaoy[proxY] == tamanhoYCobrinha[0])) {
					tamanhoCobrinha = 3;
					proxX = random.nextInt(27);
					proxY = random.nextInt(22);
					proxFruta = random.nextInt(4);
			}
				
								
			imagemMacaPodre.paintIcon(this, g, frutaPosicaox[proxX], frutaPosicaoy[proxY]);
			
			if(contador >= 8) {
				proxX = random.nextInt(27);
				proxY = random.nextInt(22);
				proxFruta = random.nextInt(4);	
				contador = 0;
				
			}	
			
			
		}
		else if(frutaID[proxFruta] == 4) {
			
			imagemBomba = new ImageIcon("Bomba.png");
				
			if((frutaPosicaox[proxX] == tamanhoXCobrinha[0] && frutaPosicaoy[proxY] == tamanhoYCobrinha[0])) {
				leste = false;
				oeste = false;
				norte = false; 
				sul = false;
				
				g.setColor(Color.red);
				g.setFont(new Font("arial", Font.BOLD, 60));
				g.drawString("Game Over", 230, 300);
				
				g.setColor(Color.white);
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("*** APERTE ENTER PARA REINICIAR ***", 200, 340);
					
			}
				
								
			imagemBomba.paintIcon(this, g, frutaPosicaox[proxX], frutaPosicaoy[proxY]);
			
			if(contador >= 8) {
				proxX = random.nextInt(27);
				proxY = random.nextInt(22);
				proxFruta = random.nextInt(4);	
				contador = 0;
				
			}						
		}
			
		
		
		//game over
		
		if(cobrinhaID != 2) {
			for(int proxY = 1; proxY < 12; proxY++)
			if((375 == tamanhoXCobrinha[0] && barreiray[proxY] == tamanhoYCobrinha[0])) {
				leste = false;
				oeste = false;
				norte = false; 
				sul = false;
				
				g.setColor(Color.red);
				g.setFont(new Font("arial", Font.BOLD, 60));
				g.drawString("Game Over", 230, 300);
				
				g.setColor(Color.white);
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("*** APERTE ENTER PARA REINICIAR ***", 200, 340);
					
			}
			for(int proxX = 1; proxX < 18; proxX++)
				if((barreirax[proxX] == tamanhoXCobrinha[0] && 275 == tamanhoYCobrinha[0])) {
					leste = false;
					oeste = false;
					norte = false; 
					sul = false;
					
					g.setColor(Color.red);
					g.setFont(new Font("arial", Font.BOLD, 60));
					g.drawString("Game Over", 230, 300);
					
					g.setColor(Color.white);
					g.setFont(new Font("arial", Font.BOLD, 20));
					g.drawString("*** APERTE ENTER PARA REINICIAR ***", 200, 340);
						
				}
		}
		
		for(int b = 1; b < tamanhoCobrinha; b++) {
			if(tamanhoXCobrinha[b] == tamanhoXCobrinha[0] && tamanhoYCobrinha[b] == tamanhoYCobrinha[0]) {
				leste = false;
				oeste = false;
				norte = false; 
				sul = false;
				
				g.setColor(Color.red);
				g.setFont(new Font("arial", Font.BOLD, 60));
				g.drawString("Game Over", 230, 300);
				
				g.setColor(Color.white);
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("*** APERTE ENTER PARA REINICIAR ***", 200, 340);
				
				
				
			}
		}
		g.dispose();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		timer.start();
		if(leste) {
			for(int r = tamanhoCobrinha-1; r >= 0; r--) {
				tamanhoYCobrinha[r+1] = tamanhoYCobrinha[r];
			}
			for(int r = tamanhoCobrinha-1; r >= 0; r-- ) {
				if(r==0) {
					tamanhoXCobrinha[r] = tamanhoXCobrinha[r] +25;
				}
				else {
					tamanhoXCobrinha[r] = tamanhoXCobrinha[r-1];
				}
				if(tamanhoXCobrinha[r] > 725) {
					tamanhoXCobrinha[r] = 25;
				}
			}
			
			repaint();
			
		}
		if(oeste) {
			for(int r = tamanhoCobrinha-1; r >= 0; r--) {
				tamanhoYCobrinha[r+1] = tamanhoYCobrinha[r];
			}
			for(int r = tamanhoCobrinha-1; r >= 0; r-- ) {
				if(r==0) {
					tamanhoXCobrinha[r] = tamanhoXCobrinha[r] -25;
				}
				else {
					tamanhoXCobrinha[r] = tamanhoXCobrinha[r-1];
				}
				if(tamanhoXCobrinha[r] < 25) {
					tamanhoXCobrinha[r] = 725;
				}
			}
			
			repaint();
			
		}
			
		if(norte) {
			for(int r = tamanhoCobrinha-1; r >= 0; r--) {
				tamanhoXCobrinha[r+1] = tamanhoXCobrinha[r];
			}
			for(int r = tamanhoCobrinha-1; r >= 0; r-- ) {
				if(r==0) {
					tamanhoYCobrinha[r] = tamanhoYCobrinha[r] -25;
				}
				else {
					tamanhoYCobrinha[r] = tamanhoYCobrinha[r-1];
				}
				if(tamanhoYCobrinha[r] < 25) {
					tamanhoYCobrinha[r] = 575;
				}
			}
			
			repaint();
			
		}
		if(sul) {
			for(int r = tamanhoCobrinha-1; r >= 0; r--) {
				tamanhoXCobrinha[r+1] = tamanhoXCobrinha[r];
			}
			for(int r = tamanhoCobrinha-1; r >= 0; r-- ) {
				if(r==0) {
					tamanhoYCobrinha[r] = tamanhoYCobrinha[r] +25;
				}
				else {
					tamanhoYCobrinha[r] = tamanhoYCobrinha[r-1];
				}
				if(tamanhoYCobrinha[r] > 575) {
					tamanhoYCobrinha[r] = 25;
				}
			}
			
			repaint();
			
		}
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		
		//mudando cobrinhas
		if(e.getKeyCode() == KeyEvent.VK_1) {
			cobrinhaID = 1;
		}
		if(e.getKeyCode() == KeyEvent.VK_2) {
			cobrinhaID = 2;
		}
		if(e.getKeyCode() == KeyEvent.VK_3) {
			cobrinhaID = 3;
		}
		
		//apos a tela de GAME OVER
		if(e.getKeyCode() == KeyEvent.VK_ENTER) {
			movimento = 0;
			pontuacao = 0;
			contador = 0;
			tamanhoCobrinha = 3;
			repaint();
			
		}
		
		
		//direita
		if(e.getKeyCode() ==  KeyEvent.VK_RIGHT) {
			movimento++;
			if(frutaID[proxFruta] == 1) {
				contador++;
			}
			else if(frutaID[proxFruta] == 2) {
				contador++;
			}
			else if(frutaID[proxFruta] == 3 && pontuacao > 0) {
				contador++;
			}
			else if(frutaID[proxFruta] == 4) {
				contador++;
			}
			leste = true;
			if(!oeste) {
				leste = true;
			}
			else {
				leste = false;
				oeste = true;
			}
			norte = false;
			sul = false;
		}
		//esquerda
		if(e.getKeyCode() ==  KeyEvent.VK_LEFT) {
			movimento++;
			if(frutaID[proxFruta] == 1) {
				contador++;
			}
			else if(frutaID[proxFruta] == 2) {
				contador++;
			}
			else if(frutaID[proxFruta] == 3 && pontuacao > 0) {
				contador++;
			}
			else if(frutaID[proxFruta] == 4) {
				contador++;
			}
			oeste = true;
			if(!leste) {
				oeste = true;
			}
			else {
				oeste = false;
				leste = true;
			}
			norte = false;
			sul = false;
		}
		//cima
		if(e.getKeyCode() ==  KeyEvent.VK_UP) {
			movimento++;
			if(frutaID[proxFruta] == 1) {
				contador++;
			}
			else if(frutaID[proxFruta] == 2) {
				contador++;
			}
			else if(frutaID[proxFruta] == 3 && pontuacao > 0) {
				contador++;
			}
			else if(frutaID[proxFruta] == 4) {
				contador++;
			}
			norte = true;
			if(!sul) {
				norte = true;
			}
			else {
				norte = false;
				sul = true;
			}
			leste = false;
			oeste = false;
		}
		//baixo
		if(e.getKeyCode() ==  KeyEvent.VK_DOWN) {
			movimento++;
			if(frutaID[proxFruta] == 1) {
				contador++;
			}
			else if(frutaID[proxFruta] == 2) {
				contador++;
			}
			else if(frutaID[proxFruta] == 3 && pontuacao > 0) {
				contador++;
			}
			else if(frutaID[proxFruta] == 4) {
				contador++;
			}
			sul = true;
			if(!norte) {
				sul = true;
			}
			else {
				sul = false;
				norte = true;
			}
			leste = false;
			oeste = false;
		}
		
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}

}
